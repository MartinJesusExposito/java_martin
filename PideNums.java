import java.util.Scanner;

class PideNums {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num = 1;
        do {
            System.out.println("Entra un num: ");
            try{
            num = keyboard.nextInt();
            total += num;
            }catch(Exception e){
                System.out.println("ERROOOOOOR, dato incorrecto- 0 para salir");
                keyboard.next();
            }
        } while (num>0);
        
        System.out.println("La suma es "+ total);
        keyboard.close();
    }
}
