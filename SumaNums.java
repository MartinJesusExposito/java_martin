class SumaNums {
    public static void main(String[] argumentos) {
        int total=0;
        for (String s : argumentos){
            int num = Integer.parseInt(s);
            total += num;
        }
        System.out.println("El total es "+total);
    }
}
