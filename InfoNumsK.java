import java.util.Scanner;

class InfoNumsK {

    public static void main(String[] argumentos) {
        int totalsuma = 0;
        int mayorNum = -1;
        int menorNum = 99999;
        int contador = 0;
        Scanner keyboard = new Scanner(System.in);
        int num = 1;
        do {
            System.out.println("Entra un num: ");
            try {
                num = keyboard.nextInt();
                totalsuma += num;
                ++contador;
                if (num > mayorNum) {
                    mayorNum = num;
                } 
                 if (num < menorNum) {
                  if(num==0){
                      break;
                  }
                    menorNum = num;
                  
                }
            } catch (Exception e) {
                System.out.println("ERROOOOOOR, dato incorrecto- 0 para salir");
                keyboard.next();
            }
        } while (num > 0);

        --contador;
        System.out.println(contador+ " numeros introducidos");
        System.out.println("El total es " + totalsuma);
        System.out.println("El numero mas grande es " + mayorNum);
        System.out.println("El mas pequeño es" + menorNum);
        float media = (float) totalsuma / contador;
        System.out.println("la media es " + media);
        keyboard.close();
    }

}