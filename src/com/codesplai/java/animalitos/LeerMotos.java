
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class LeerMotos {

    public static void main(String[] args) {
        File fin = new File("motos.csv");
        File dhFile = new File(args[0] + ".txt");

        Set<String> motos = new TreeSet<String>();

        try (InputStreamReader fr = new InputStreamReader(new FileInputStream(fin));
                BufferedReader br = new BufferedReader(fr);
                FileWriter fw = new FileWriter(dhFile);
                BufferedWriter bw = new BufferedWriter(fw);) {
            String linea;
            do {
                linea = br.readLine();
                if (linea != null) {
                    bw.write(linea);

                    if (linea.toLowerCase().contains(args[0])) {
     int separador;
     separador=linea.indexOf(",");
     System.out.println(separador);
                        linea = br.readLine();
                        if (linea != null) {
                            bw.newLine();
                        }
                    }
                }

            } while (linea != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (String provincia : motos) {
            System.out.println(provincia);
        }
    }
}
