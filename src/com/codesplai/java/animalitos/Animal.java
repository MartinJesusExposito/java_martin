package com.codesplai.java.animalitos;

public abstract class Animal {
    private String color = "";
    
    public void duerme() {
        System.out.println("Animal a dormir!");
    }
    
    public void come() {
        System.out.println("Animal a comer!");
    }

    public int getNumeroPatas() {
        return 0;
    }
    
    public void setColor(String color) {
       this.color = color;
    }

    public abstract void juega();
}