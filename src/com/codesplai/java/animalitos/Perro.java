package com.codesplai.java.animalitos;

class Perro extends Animal {
    public void ladra() {
        System.out.println("Guau!");
    }
    @Override public int getNumeroPatas() {
        return 4;
    }
    @Override
    public void juega() {
        System.out.println("estoy jugando");
    }
}