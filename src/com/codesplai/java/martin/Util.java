package com.codesplai.java.martin;

import java.util.Random;

public class Util {
    static String dia(int dia, String idioma) {
        String resultado = "";
        String[] es = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
        String[] ca = { "Dillunes", "Dimarts", "Dimecres", "Dijoues", "Divendres", "Dissabte", "Diumenge" };
        String[] en = { "Monday", "Tuesday", "thursday", "wednesday", "freday", "saturday", "sunday" };

        if (idioma == "es") {
            resultado = es[dia];
        } else if (idioma == "ca") {
            resultado = ca[dia];
        } else if (idioma == "en") {
            resultado = en[dia];
        } else {
            resultado = "Datos incorrectos";
        }
        return resultado;
    }

    static String mes(int mes, String idioma) {
        String resultado = "";
        String[] es = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
                "Octubre", "Noviembre", "Diciembre" };
        String[] ca = { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Septembre", "Octubre",
                "Novembre", "Deceembre" };
        String[] en = { "Genuary", "February", "March", "April", "May", "Jun", "July", "August", "September", "October",
                "November", "December" };
        if (idioma == "es") {
            resultado = es[mes];
        } else if (idioma == "ca") {
            resultado = ca[mes];
        } else if (idioma == "en") {
            resultado = en[mes];
        } else {
            resultado = "Datos incorrectos";
        }
        return resultado;
    }

    static int mayor(int[] arrayint) {
        int mayor = 0;
        for (int i = 0; i < arrayint.length; i++)

            if (mayor <= arrayint[i]) {
                mayor = arrayint[i];

            }

        return mayor;
    }

    static char mayorChar(char[] arrayChar) {
        char caracter = arrayChar[0];
        for (int i = 0; i < arrayChar.length; i++)

            if (caracter <= arrayChar[i]) {
                caracter = arrayChar[i];
            }
        return caracter;
    }

    static void muestraarray(int[] arrayint, String add) {
        for (int i = 0; i < arrayint.length; i++)
            if (i == arrayint.length - 1) {
                System.out.print(arrayint[i]);
                System.out.println();
            } else {
                System.out.print(arrayint[i] + add);
            }
    }

    static void muestraarraychar(char[] arrayint, String add) {
        for (int i = 0; i < arrayint.length; i++)
            if (i == arrayint.length - 1) {
                System.out.print(arrayint[i]);
                System.out.println();
            } else {
                System.out.print(arrayint[i] + add);
            }
    }

    static int[] pares(int[] arrayint) {
        int[] pares = new int[arrayint.length];
        int contador = 0;

        for (int posicion : arrayint) {
            if (posicion % 2 == 0) {
                pares[contador] = posicion;
                contador++;
            }
        }

        return pares;
    }

    static String pass(int caracteres) {
        String pass = "";
        int contador = 0;

        String[] abc = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
                "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
                "9" };

        while (true) {
            do {
                String letra = "";
                contador++;
                Random random = new Random();
                letra = abc[(int) (Math.random() * abc.length)];
                pass = pass + letra;
            } while (contador != caracteres);

            if (pass.matches(".*[a-z]+.*") && pass.matches(".*[A-Z]+.*") && pass.matches(".*[0-9]+.*")) {
                System.out.println("la contraseña es correcta");
                break;
            } else {
                System.out.println("la pass no es correcta vuelvo a pedirla");
                contador = 0;
                pass = "";
            }

        }
        return pass;
    }

    static boolean verificastring(String email) {
        boolean verificado = false;

        if (email.matches("...+@\\w+\\.\\w+")) {
            verificado = true;

        } 

        return verificado;
    }

    static boolean verificapass(String pass){
        boolean verificado= false;

          if (pass.matches(".*[a-z]+.*") && pass.matches(".*[A-Z]+.*") && pass.matches(".*[0-9]+.*")) {
            verificado=true;
          }

          return verificado;

    }


    static String encripta(String original,String Seguridad){
        String modificada="";
        char letra_max=Seguridad.charAt(0);
        char letra_min=Seguridad.charAt(0);
        int diferencia=0;
        for(int i=0;i<Seguridad.length();i++){
            char letra=Seguridad.charAt(i);
            if(letra_max<letra){
                letra_max=letra;
            }else if(letra_min>letra){
                letra_min=letra;
            }

diferencia=letra_max-letra_min;
        }  for(int i=0;i<original.length();i++){
            char letra=original.charAt(i);
            letra = (char)(letra+ (char)diferencia);
            modificada= modificada+letra;

        }


        return modificada;
    }

    static String deencripta(String original,String Seguridad){
        String modificada="";
        char letra_max=Seguridad.charAt(0);
        char letra_min=Seguridad.charAt(0);
        int diferencia=0;
        for(int i=0;i<Seguridad.length();i++){
            char letra=Seguridad.charAt(i);
            if(letra_max<letra){
                letra_max=letra;
            }else if(letra_min>letra){
                letra_min=letra;
            }

diferencia=letra_max-letra_min;
        }  for(int i=0;i<original.length();i++){
            char letra=original.charAt(i);
            letra = (char)(letra- (char)diferencia);
            modificada= modificada+letra;

        }


        return modificada;
    }
}
