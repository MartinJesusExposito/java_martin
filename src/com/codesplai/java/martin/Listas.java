package com.codesplai.java.martin;


class Listas {

    public static void main(String[] args) {

        int[] lista_numeros = { 10, 58, 47, 2, 6, 74, 25 };
        // String girado = " ";
        String ciudad = "Constantinopla";

        for (int numero : lista_numeros) {
            System.out.print(numero + " - ");
        }

        // for (char letra : ciudad.toCharArray()) {
        //     // girado = girado + letra;
        //     // System.out.println(girado);
        // }
        for (int i = ciudad.length(); i > 0; i--) {
            System.out.print(ciudad.charAt(i - 1));
        }

    }
}