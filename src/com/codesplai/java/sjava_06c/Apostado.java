package com.codesplai.java.sjava_06c;

class Apostado{

    String juego;
    int cantidad;
    int numerodeapuesta;

    public Apostado(int numerodeapuesta,String juego,int cantidad) {
        this.numerodeapuesta=numerodeapuesta;
        this.juego = juego;
        this.cantidad = cantidad;
    } 

    @Override
    public String toString() {
        return "Apuesta nunmero :"+numerodeapuesta+ " apuestas " + this.juego + " y la cantidad " + this.cantidad;
    }
}