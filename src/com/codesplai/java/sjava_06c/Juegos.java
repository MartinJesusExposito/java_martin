package com.codesplai.java.sjava_06c;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);

    /**
     * pide nombre de jugador, 1 o 2 crea objeto "Jugador" y lo asigna a jugador1 o
     * jugador2
     */
    public static void pideJugador(int numJugador) {
        // pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        } else {
            Juegos.jugador2 = j;
        }
    }

    /**
     * muestra menu de juegos y pide opción
     */
    public static void menu() {
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedra, Papel o Tijeras ");
        System.out.println("3: Ruleta");
        System.out.println("*******************");
        System.out.println("4: RuletaTest");
        System.out.println("*******************");
        // pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
        } while (opcion < 1 || opcion > 4);

        switch (opcion) {
        case 1:
            Juegos.caraCruz();
            break;
        case 2:
            Juegos.PPT();
            break;
            case 3:
            Juegos.Ruleta();
            break;
            case 4:
            Juegos.Ruleta2();
            break;
        default:
            break;
        }
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Cara (C) o cruz(X) ?  ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    public static void PPT() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego
        int resultado;
        String ganado = "";
        String perdido = "";

        System.out.println();
        System.out.println("Piedra Papel o Tijeras:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Piedra(1),Papel(2) o tijeras(3)??? ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            int respuesta = rnd.nextInt(3) + 1;
            resultado = Integer.parseInt(apuesta);
            // resultado= String.valueOf(respuesta);
            if (resultado == 1 && respuesta == 2 || resultado == 2 && respuesta == 3
                    || resultado == 3 && respuesta == 1) {
                switch (resultado) {
                case 1:
                    ganado = "Piedra";
                    perdido = "Tijeras";
                    break;
                case 2:
                    ganado = "Papel";
                    perdido = "Piedra";
                    break;
                case 3:
                    ganado = "Tijeras";
                    perdido = "Papel";
                    break;

                }
                System.out.println(" Has ganado! xk has metido " + ganado + " Y el ha metido " + perdido);
                Juegos.jugador1.ganadas++;
            } else if (resultado == respuesta) {
                System.out.println(" Empate! por lo tanto no ganas ");
                Juegos.jugador1.empates++;
            } else {
                switch (resultado) {
                case 1:
                    ganado = "Piedra";
                    perdido = "Papel";
                    break;
                case 2:
                    ganado = "Papel";
                    perdido = "Tijeras";
                    break;
                case 3:
                    ganado = "Tijeras";
                    perdido = "Piedra";
                    break;

                }
                System.out.println(" Has perdido! xk has metido " + ganado + " Y el ha metido " + perdido);

            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas, %d empates, %d perdidas.",
                Juegos.jugador1.nombre, Juegos.jugador1.partidas, Juegos.jugador1.ganadas, Juegos.jugador1.empates,
                Juegos.jugador1.partidas - (Juegos.jugador1.empates + Juegos.jugador1.ganadas));
        System.out.println(resumen);
    }

    public static void Ruleta() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 1; // partidas por defecto en este juego
        int resultado;
        int euros = 100;
        int apuestas=0;
        int perdido=0;
        int[] rojos = { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};
        System.out.println();
        System.out.println("RULETAAAAAAAA:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partida", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        do {
            System.out.println("Apuesta lo que quieras! tienes " + euros + " euros");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            int respuesta = rnd.nextInt(37);
            int resultadorojo = Arrays.binarySearch(rojos, respuesta);
            resultado = Integer.parseInt(apuesta);
            apuestas++;
            perdido= perdido+resultado;
            euros = euros - resultado;
            System.out.println("Te has quedado con : " + euros + " euros");
            if (respuesta == 0) {
                System.out.println("0! GANA LA BANCAAA");

            } else {
                System.out.println("A donde quieres apostar?? Rojo/Negro(c) a pares/impares?(p) o falta/pasa(f)??");
                String Adonde = keyboard.next();
                if (Adonde.equals("c")) {
                    System.out.println("Rojo(r) o Negro(n)?");
                    String Color = keyboard.next();
                    if (Color.equals("r") && resultadorojo != -1) {
                        euros = euros + (resultado * 2);
                        System.out.println("has ganado! ahora tienes " + euros + ", Ha salido el numero: " + respuesta);
                    } else if (Color.equals("n") && resultadorojo == -1) {
                        System.out.println("has ganado! ahora tienes " + euros + ", Ha salido el numero: " + respuesta);
                        euros = euros + (resultado * 2);
                    } else {
                        System.out.println("has perdido. Ha salido el numero :" + respuesta);
                    }

                }
                if (Adonde.equals("p")) {
                    System.out.println("Pares(p) o nones(n)?");
                    String Color = keyboard.next();
                    if (Color.equals("p") && respuesta % 2 == 0) {
                        euros = euros + (resultado * 2);
                        System.out.println("has ganado! ahora tienes " + euros + ". Ha salido el numero: " + respuesta);
                        Juegos.jugador1.ganadas++;
                    } else if (Color.equals("n") && respuesta % 2 == 0) {
                        euros = euros + (resultado * 2);
                        System.out.println("has ganado! " + respuesta);
                        System.out.println("has ganado! ahora tienes " + euros + ". Ha salido el numero: " + respuesta);
                        Juegos.jugador1.ganadas++;
                    } else {
                        System.out.println("has perdido. Ha salido el numero: " + respuesta);
                    }

                }
                if (Adonde.equals("f")) {
                    System.out.println("falta(f) o pasa(p)?");
                    String Color = keyboard.next();
                    if (Color.equals("f") && respuesta < 18) {
                        euros = euros + (resultado * 2);
                        System.out.println("has ganado! ahora tienes " + euros + ".Ha salido el numero: " + respuesta);
                        Juegos.jugador1.ganadas++;
                    } else if (Color.equals("p") && respuesta > 18) {
                        euros = euros + (resultado * 2);
                        System.out.println("has ganado! " + respuesta);
                        System.out.println("has ganado! ahora tienes " + euros + ". Ha salido el numero: " + respuesta);
                        Juegos.jugador1.ganadas++;
                    } else {
                        System.out.println("has perdido. Ha salido el numero " + respuesta);
                        
                    }

                }
            }
            // resultado= String.valueOf(respuesta);
        } while (euros > 0);

        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d apuestas, %d ganadas, %d perdidas, %d total perdido.",
                Juegos.jugador1.nombre, 
                apuestas,
                 Juegos.jugador1.ganadas, 
                 apuestas-Juegos.jugador1.ganadas,
                perdido);
        System.out.println(resumen);
    }

    public static void Ruleta2() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 1; // partidas por defecto en este juego
        int resultado;
        boolean seguirapostando=true;
        int euros = 100;
        int apuestas=0;
        int perdido=0;
        int[] rojos = { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};
        System.out.println();
        System.out.println("RULETAAAAAAAA:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partida", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        do {
            Collection<Apostado> jugadas= new ArrayList<Apostado>();

            do{
            System.out.println("Apuesta lo que quieras! tienes " + euros + " euros");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            apuestas++;
            String nombreapuesta = "apuesta"+apuestas;
            resultado = Integer.parseInt(apuesta);
            perdido= perdido+resultado;
            euros = euros - resultado;
            System.out.println("Te has quedado con : " + euros + " euros");
            System.out.println("A donde quieres apostar?? Rojo/Negro(c) a pares/impares?(p) o falta/pasa(f)??");
            String Adonde = keyboard.next();
            Apostado aaaa = new Apostado(apuestas,Adonde,resultado);
            jugadas.add(aaaa);
            
            for(Apostado elem:jugadas){
                System.out.println(elem);
            }
            // System.out.println(jugadas);
String Seguir = keyboard.next();
if(Seguir=="si"){
    System.out.println("Continuamos apostando");
}else{
    seguirapostando=false;
}

        }while(seguirapostando==true);            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            int respuesta = rnd.nextInt(37);
            int resultadorojo = Arrays.binarySearch(rojos, respuesta);
            if (respuesta == 0) {
                System.out.println("0! GANA LA BANCAAA");

            } else {
               
                // if (Adonde.equals("c")) {
                //     System.out.println("Rojo(r) o Negro(n)?");
                //     String Color = keyboard.next();
                //     if (Color.equals("r") && resultadorojo != -1) {
                //         euros = euros + (resultado * 2);
                //         System.out.println("has ganado! ahora tienes " + euros + ", Ha salido el numero: " + respuesta);
                //     } else if (Color.equals("n") && resultadorojo == -1) {
                //         System.out.println("has ganado! ahora tienes " + euros + ", Ha salido el numero: " + respuesta);
                //         euros = euros + (resultado * 2);
                //     } else {
                //         System.out.println("has perdido. Ha salido el numero :" + respuesta);
                //     }

                // }
                // if (Adonde.equals("p")) {
                //     System.out.println("Pares(p) o nones(n)?");
                //     String Color = keyboard.next();
                //     if (Color.equals("p") && respuesta % 2 == 0) {
                //         euros = euros + (resultado * 2);
                //         System.out.println("has ganado! ahora tienes " + euros + ". Ha salido el numero: " + respuesta);
                //         Juegos.jugador1.ganadas++;
                //     } else if (Color.equals("n") && respuesta % 2 == 0) {
                //         euros = euros + (resultado * 2);
                //         System.out.println("has ganado! " + respuesta);
                //         System.out.println("has ganado! ahora tienes " + euros + ". Ha salido el numero: " + respuesta);
                //         Juegos.jugador1.ganadas++;
                //     } else {
                //         System.out.println("has perdido. Ha salido el numero: " + respuesta);
                //     }

                // }
                // if (Adonde.equals("f")) {
                //     System.out.println("falta(f) o pasa(p)?");
                //     String Color = keyboard.next();
                //     if (Color.equals("f") && respuesta < 18) {
                //         euros = euros + (resultado * 2);
                //         System.out.println("has ganado! ahora tienes " + euros + ".Ha salido el numero: " + respuesta);
                //         Juegos.jugador1.ganadas++;
                //     } else if (Color.equals("p") && respuesta > 18) {
                //         euros = euros + (resultado * 2);
                //         System.out.println("has ganado! " + respuesta);
                //         System.out.println("has ganado! ahora tienes " + euros + ". Ha salido el numero: " + respuesta);
                //         Juegos.jugador1.ganadas++;
                //     } else {
                //         System.out.println("has perdido. Ha salido el numero " + respuesta);
                        
                //     }

                // }
            }
            // resultado= String.valueOf(respuesta);
        } while (euros > 0);

        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d apuestas, %d ganadas, %d perdidas, %d total perdido.",
                Juegos.jugador1.nombre, 
                apuestas,
                 Juegos.jugador1.ganadas, 
                 apuestas-Juegos.jugador1.ganadas,
                perdido);
        System.out.println(resumen);
    }

}



