public class Libreria{

    public int doblar(int numero){
        numero = numero*2;
        return numero;
    }

    public int doblaEdad(Persona p1){
        p1.edad= p1.edad*2;
        return p1.edad;
    }
}