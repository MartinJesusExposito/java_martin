class InfoNums {
    public static void main(String[] argumentos) {
        int totalnums = 0;
        int totalsuma = 0;
        int mayorNum = Integer.parseInt(argumentos[0]);
        int menorNum = Integer.parseInt(argumentos[0]);
     

        for (String s : argumentos) {
            int num = Integer.parseInt(s);
            totalsuma += num;
            totalnums += 1;
            if (num > mayorNum) {
                mayorNum = num;
            }
            else if (num < menorNum) {
                menorNum = num;
            }
        }

        float media = (float) totalsuma / totalnums;
        System.out.println(totalnums + " numeros introducidos");
        System.out.println("El total es " + totalsuma);
        System.out.println("El numero mas grande es " + mayorNum);
        System.out.println("El mas pequeño es" + menorNum);
        System.out.println("la media es " + media);

    }
}
